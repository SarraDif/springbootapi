# springBootAPI

CRUD API for reading messages using Java, Spring Boot, H2, JPA.

## Requirements


 + Java -> 1.8.x
 + Maven -> 3.x.x

## How to Run


** 1. Clone the application **

```bash
git clone https://SarraDif@bitbucket.org/SarraDif/springbootapi.git
```

** 2. Build and run the app using maven **

+ Create .jar package

```bash
		mvn package
		java -jar target/messages.jar
```

+ Execute in command line
```bash
		mvn spring-boot:run
```

The app will start running at <http://localhost:8080>.


## Explore the APIs


The app defines following CRUD APIs.

 + Read All messages -> GET ["/messages"](http://localhost:8080/messages)


 + Read One message -> GET ["/messages/{messageId}"](http://localhost:8080/messages/{messageId})


 + Create a message -> POST ["/messages"](http://localhost:8080/messages)


 + Update a message -> PUT ["/messages/{messageId}"](http://localhost:8080/messages/{messageId})


 + Delete a message -> DELETE ["/messages/{messageId}"](http://localhost:8080/messages/{messageId})



You can test them using postman or any other rest client.