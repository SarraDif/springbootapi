package com.springbootapp.springbootapi.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService {
	@Autowired
	private MessageRepository	messageRepository;

	public List<Message> getAllMessages() {
		List<Message> messages = new ArrayList<>();

		messageRepository.findAll().forEach(messages::add);
		return (messages);
	}

	public Message getMessage(int id) {
		Optional<Message> message = messageRepository.findById(id);

		if (!message.isPresent()) {
			throw new MessageNotFoundException("Requested ID (" + id + ") not found.");
		}
		return (message.get());
	}

	public void addMessage(Message message) {
		messageRepository.save(message);
	}

	public void updateMessage(int id, Message updated) {
		Optional<Message> message = messageRepository.findById(id);

		if (!message.isPresent()) {
			addMessage(updated);
		}
		updated.setId(id);
		messageRepository.save(updated);
	}

	public void deleteMessage(int id) {
		Optional<Message> message = messageRepository.findById(id);

		if (!message.isPresent()) {
			throw new MessageNotFoundException("Requested ID (" + id + ") not found.");
		}
		messageRepository.deleteById(id);
	}
}
