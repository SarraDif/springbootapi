package com.springbootapp.springbootapi.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessageController {

	@Autowired
	private MessageService	messageService;


	@GetMapping("/messages")
	public List<Message> getAllMessages() {
		return (messageService.getAllMessages());
	}

	@GetMapping("/messages/{id}")
	public Message	getMessage(@PathVariable int id) {
		return (messageService.getMessage(id));
	}

	@PostMapping("/messages")
	@ResponseStatus(value = HttpStatus.CREATED)
	public void		addMessage(@RequestBody Message message) {
		messageService.addMessage(message);
	}

	@PutMapping("/messages/{id}")
	public void		updateMessage(@RequestBody Message message, @PathVariable int id) {
		messageService.updateMessage(id, message);
	}

	@DeleteMapping("/messages/{id}")
	public void		deleteMessage(@PathVariable int id) {
		messageService.deleteMessage(id);
	}
}
